# PEC 1 - Un juego de carreras

## FUNCIONAMIENTO

El juego se inicia con una carrera de tres vueltas. Cuando se termina, se muestra la repetición de la carrera con varias cámaras, y después se juega otra carrera contra el fantasma guardado de la carrera anterior.


## ESTRUCTURA

1 - Creación del terreno (Terrain1 - Terrain4), de un cuerpo de agua (WaterProDaytime) y del circuito (ER Road Network) junto con los puntos clave (Waypoint001 - Waypoint028).

2 - Creación del coche principal (Car):
- Si el coche se sale de la carretera, desciende su velocidad (script Controlador).
- Funcionalidad para grabar la carrera del coche (script GrabarVuelta).
- Creación del ScriptableObject 'Datos Vuelta Fantasma' (script DatosVueltaFantasma).

3 - Creación de un coche rival automático (oculto) (CarWaypointBased).
4 - Creación de un coche fantasma para reproducir la carrera del coche principal (CocheFantasma1).
- Funcionalidad (script LeerDatosVuelta, GhostLapDataSO1 y script ControladorDatosVueltaFantasma).

5 - Elementos de interfaz:
- Muestra del tiempo de cada vuelta (Vuelta1 - Vuelta3) y del récord (Record).
- Funcionalidad (script ControladorDatosVuelta):
- Conteo del tiempo.
- Completar una vuelta.
- Repetición de la carrera.
- Reiniciar la carrera para correr contra el fantasma.
			
- Cuando se está reproduciendo la repetición de la carrera, se muestra un texto (Repeticion) y un botón para saltarse la repetición (BotonContinuar).
- Funcionalidad (script ContinuarCarrera).

- Al terminar el juego, aparece el botón Salir para cerrar la aplicación (BotonSalir).
- Funcionalidad (script Salir).

6 - Creación de muros, uno en medio del circuito (MediaVuelta) y otro en la meta (VueltaCompleta) para controlar que se pase por ambos.
- Funcionalidad (scripts MediaVuelta y VueltaCompleta).

7 - Seguimiento del coche y visionado de la repetición de la carrera. 
- Creación de una cámara (MultipurposeCameraRig) que sigue al coche.
- Creación de 6 cámaras de televisión (CctvCamera1 - CctvCamera6) que se activan en la repetición.
- Funcionalidad de la repetición de la carrera (script RepeticionCamaras).

8 - Se han creado dos escenas con circuitos distintos.
9 - Se ha añadido música a las escenas.
10 - Se ha añadido un atajo a la carretera (ER Road Network Atajo).
11 - Se ha creado un menú desde el que se puede elegir el circuito (Menu).
- Funcionalidad (script Menu).

Nota: aunque se ha implementado parte de la funcionalidad para hallar la vuelta rápida, se ha decidido finalmente mostrar toda la carrera guardada.

